from django.db import models

# Create your models here.


class PositiveWord(models.Model):
    word = models.CharField(max_length=50, primary_key=True)


class NegativeWord(models.Model):
    word = models.CharField(max_length=50, primary_key=True)


class TweetSentiment(models.Model):
    tweet_id = models.BigIntegerField(primary_key=True)
    sentiment_score = models.IntegerField()
