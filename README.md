# SENDAL

Implementasi mini projek MPPI

## Author

Agnes Handoko -- 1706039761

Fakhira Devina -- 1717069792210

Febriananda Wida Pramudita -- 1706028631

Ferriyal bin Yahya -- 1706979240

Wira Abdillah Siregar -- 1506690630


## About

Berikut adalah sentiment analysis sederhana yang ditulis dengan bahasa python yang ditanamkan pada website django. Aplikasi ini dibuat untuk memenuhi tugas mini riset MPPI.



## Menjalankan di local

register ke developer twitter, dan taruh environment variable di file .env seperti berikut
```
access_token=<AKSES_TOKEN_ANDA>
access_token_secret=<AKSES_TOKEN_SECRET_ANDA>
API_key=<API_KEY_ANDA>
API_secret_key=<API_SECRET_KEY_ANDA>
```

```
pipenv shell # load environment
python load_database.py # meload database
python manage.py runserver # menjalankan aplikasi sentiment analysis yang terdeploy pada website django. Untuk production, gunakan wsgi
```

## Cara Menggunakan

Aplikasi akan menggumpulkan 999 tweet bahasa indonesia terbaru dan menjalankan sentiment analysis. Agar berjalan lebih cepat, hasil sentiment analysis akan di-cache. Untuk mengupdate, masukkan parameter update=true saat request. Contoh 'http://sendal-mppi-sukses.herokuapp.com/index.html?update=true'